package main

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

type DatabaseConfig struct {
	Host     string `yaml:"host"`
	User     string `yaml:"user"`
	Name     string `yaml:"name"`
	Password string `yaml:"password"`
}

func CreateDB(config *DatabaseConfig) (*sqlx.DB, error) {
	db, err := sqlx.Connect("postgres", makeConnStr(config))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create database connection")
	}

	if _, err := db.Exec(SCHEMA); err != nil {
		return nil, errors.Wrap(err, "failed to exec DB schema")
	}

	return db, nil
}


func makeConnStr(config *Config) string {
	return fmt.Sprintf(
		"host=%s user=%s dbname=%s sslmode=disable password=%s",
		config.Host,
		config.User,
		config.Name,
		config.Password,
	)
}
