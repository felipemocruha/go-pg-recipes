postgres:
	docker run -d --network=host -e POSTGRES_USER=recipes -e POSTGRES_PASSWORD=recipes -e POSTGRES_DB=recipes -e PGDATA=/var/lib/postgresql/data/pgdata postgres:12-alpine
