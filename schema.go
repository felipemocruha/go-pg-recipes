package main

const SCHEMA = `
CREATE EXTENSION IF NOT EXISTS "fuzzystrmatch";
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "lo";

CREATE TABLE IF NOT EXISTS thing (
    id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
    seq serial,
    seq_big bigserial,
    name text,
    count int,
    double_count numeric GENERATED ALWAYS AS (count * 2.0) STORED,
    metadata jsonb,
    attachment lo
);

-- PAGINATION (reddit like)
CREATE INDEX index_thing_on_seq ON thing USING btree (seq);

-- SHOW MOST RECENT FIRST
SELECT * FROM thing WHERE seq < $2 ORDER BY seq DESC LIMIT $3;

-- JSON FIELD INSERT
INSERT INTO thing(metadata) VALUES('{"haha": "sua_tia", "b": true, "c": []}');


-- TRIGGER
CREATE OR REPLACE FUNCTION thing_created_notify() RETURNS trigger AS $$
  DECLARE
    payload text;
  BEGIN
    payload := (
      SELECT row_to_json(r) FROM (
        SELECT id FROM thing WHERE id=NEW.id
      ) r
    );
    PERFORM pg_notify('thing_created', payload);
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

-- Partitioned table
-- some monthly table | TODO: inspect inserts and queries

CREATE TABLE something (
    id uuid DEFAULT uuid_generate_v4(),
    value int,
    created_at TIMESTAMP not null DEFAULT now()
) PARTITION BY RANGE (created_at);

CREATE TABLE something_2020_04 PARTITION OF something
  FOR VALUES FROM ('2020-04-01') TO ('2020-04-30');
`

const TRIGGERS = `
CREATE TRIGGER thing_created AFTER INSERT ON thing
  FOR EACH ROW EXECUTE PROCEDURE thing_created_notify();
`
